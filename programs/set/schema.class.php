<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";
require_once dirname(__FILE__).'/../functions.php';

global $babDB;

$LibOrm = bab_Functionality::get('LibOrm');
/*@var $LibOrm Func_LibOrm */

$LibOrm->initMysql();
ORM_MySqlRecordSet::setBackend(new ORM_MySqlBackend($babDB));




/**
 *
 * @property ORM_UserField		$user
 * @property ORM_IntField		$ov_schema
 * @property ORM_EnumField		$type
 * @property ORM_BoolField		$follow_order
 *
 */
class as_SchemaSet extends ORM_MySqlRecordSet
{
    const type_name 		= 0;
    const type_function 	= 1;
    const type_group 		= 2;

    public function __construct()
    {
        parent::__construct();

        $this->setPrimaryKey('id');

        $this->addFields(
            ORM_StringField('name'),
            ORM_StringField('description'),
            ORM_UserField('user'),
            ORM_IntField('ov_schema')->setDescription('The id when editing an already existing schema'),
            ORM_EnumField('type', as_SchemaSet::getTypes()),
            ORM_BoolField('follow_order'),
            ORM_IntField('org_chart')
        );

        $this->hasMany('step', 'as_SchemaStepSet', 'schema');
    }


    public static function getTypes($type = null)
    {
        $types = array(
            as_SchemaSet::type_name 	=> bab_translate('Nominative schema'),
            as_SchemaSet::type_function	=> bab_translate('Staff schema'),
            as_SchemaSet::type_group	=> bab_translate('Group schema')
        );
        if($type !== null){
            return $types[$type];
        }
        return $types;
    }
}




/**
 *
 * @property ORM_UserField		$user
 * @property ORM_IntField		$ov_schema
 * @property ORM_EnumField		$type
 * @property ORM_BoolField		$follow_order
 *
 */
class as_Schema extends ORM_MySqlRecord
{
    public function sendToOVidentia()
    {
        $schemaStepSet = new as_SchemaStepSet();
        $schemaStepEntitySet = new as_SchemaStepEntitySet();

        $formula = '';

        $steps = $schemaStepSet->select($schemaStepSet->schema->is($this->id));
        foreach($steps as $step){
            $entities = $schemaStepEntitySet->select($schemaStepEntitySet->step->is($step->id));
            $stepFormula = '';
            $separator = '|';
            if($step->all){
                $separator = '&';
            }
            foreach($entities as $entity){
                if($stepFormula == ''){
                    $stepFormula = $entity->entity;
                }else{
                    $stepFormula.= $separator.$entity->entity;
                }
            }
            if($stepFormula != ''){
                if($formula == ''){
                    $formula = $stepFormula;
                }else{
                    $formula.= ','.$stepFormula;
                }
            }
        }
        if($this->ov_schema){
            $ov_schema = $this->ov_schema;
        }else{
            $ov_schema = null;
        }
        return bab_WFSetApprobationInfos($ov_schema, $this->name, $this->description, $formula, 'Y', $this->type, $this->org_chart);
    }
}