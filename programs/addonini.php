;<?php /*

[general]
name							="approbation_schema"
version							="0.2.5"
addon_type						="EXTENSION"
encoding						="UTF-8"
mysql_character_set_database	="latin1,utf8"
description						="Approbation schema management addon"
description.fr					="Module fournissant une interface de création/modification des schémas d'approbation"
delete							=1
ov_version						="8.4.91"
php_version						="5.1.0"
addon_access_control			="0"
author							="Cantico"
icon							="icon.png"
tags							="extension,default,approbation"

[addons]
widgets							="1.0.68"
LibTranslate					=">=1.12.0rc3.01"
LibOrm							="0.9.0"

;*/