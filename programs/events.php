<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
include_once 'base.php';
require_once dirname(__FILE__).'/functions.php';


/**
 *
 */
function approbation_schema_onBeforeSiteMapCreated(bab_eventBeforeSiteMapCreated $event)
{

    require_once $GLOBALS['babInstallPath'].'utilit/delegincl.php';

    $addon = bab_getAddonInfosInstance('approbation_schema');
    bab_functionality::includeOriginal('Icons');
    bab_debug('test');
    if (bab_isUserAdministrator() || bab_getCurrentAdmGroup() != 0) { // sitemap is not modified when when delegation is modified

        $item = $event->createItem('babAdminApprob'); // use the same ID as the previous link in core because of the special handling of delegated functionalities in OFSitemapMenu or section display
        $item->setLabel(as_translate('Approbation schemas'));
        $item->setLink($addon->getUrl().'main&idx=schema.displaylist');
        $item->addIconClassname(Func_Icons::APPS_APPROBATIONS);
        $item->setPosition(array('root', 'DGAll', 'babAdmin', 'babAdminSection'));

        $event->addFunction($item);
    }
}
