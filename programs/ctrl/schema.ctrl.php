<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';
require_once dirname(__FILE__).'/../ui/schema.ui.php';

require_once dirname(__FILE__).'/../set/schema.class.php';
require_once dirname(__FILE__).'/../set/schemastep.class.php';
require_once dirname(__FILE__).'/../set/schemastepentity.class.php';

require_once $GLOBALS['babInstallPath']."utilit/wfincl.php";
require_once $GLOBALS['babInstallPath']."utilit/ocapi.php";


bab_functionality::includeOriginal('Icons');


/**
 *
 */
class as_CtrlSchema extends as_Controller
{
    public function displayList()
    {
        if (!bab_isUserAdministrator() && !bab_isDelegated('approbations')) {
            throw new bab_AccessException(as_translate('Access denied'));
        }
        
        $W = bab_Widgets();
        $page = $W->BabPage();
        $page->addStyleSheet('addons/approbation_schema/approbation_schema.css');

        $page->addItemMenu('approbation_schema', as_translate('Schemas list'), as_Controller()->schema()->displayList()->url());
        $page->addItemMenu('nominative', bab_translate('Nominative schema'), as_Controller()->schema()->edit(as_SchemaSet::type_name)->url());
        $page->addItemMenu('staff', bab_translate('Staff schema'), as_Controller()->schema()->edit(as_SchemaSet::type_function)->url());
        $page->addItemMenu('group', bab_translate('Group schema'), as_Controller()->schema()->edit(as_SchemaSet::type_group)->url());

        $page->setCurrentItemMenu('approbation_schema');

        $schemas = bab_WFGetApprobationsList();

        $header = array(
            'name' => as_translate('Name'),
            'description' => as_translate('Description'),
            'type' => as_translate('Type'),
            'id_oc' => as_translate('Orghart'),
            '_actions' => ''
        );

        $content = array();
        foreach($schemas as $schema) {
            $content[$schema['id']] = array(
                'name' => $schema['name'],
                'description' => $schema['description'],
                'type' => $schema['type'],
                'id_oc' => $schema['id_oc'],
                '_actions' => $schema['id']
            );
        }

        $table = new as_TableArrayView($header, $content, 'approbation_schema');
        $table->addClass(Func_Icons::ICON_LEFT_16);

        $page->addItem($table);

        return $page;
    }


    private function convertSchema($formula, as_Schema $schema){
        $stepSet = new as_SchemaStepSet();
        $entitySet = new as_SchemaStepEntitySet();

        if(!$schema->follow_order){
            if(strpos($formula, '|') || !strpos($formula, '&')){
                $formula = str_replace(',', '|', $formula);
                $formula = str_replace('&', '|', $formula);
            }else{
                $formula = str_replace(',', '&', $formula);
            }
        }

        $steps = explode(',', $formula);
        $order = 0;
        foreach($steps as $step){
            $order++;

            $stepORM = $stepSet->newRecord();
            $stepORM->order = $order;
            $stepORM->schema = $schema->id;

            $entities = explode('&', $step);
            if(isset($entities[1])){
                $stepORM->all = true;
            }else{
                $entities = explode('|', $step);
                $stepORM->all = false;
            }

            $stepORM->save();
            foreach($entities as $entity){
                $entityORM = $entitySet->newRecord();
                $entityORM->step = $stepORM->id;
                $entityORM->entity = $entity;
                $entityORM->name = as_getEntityName($schema->type, $entity);

                $entityORM->save();
            }
        }
    }


    private function getNewSchema($schemaId, $currentId, $type){
        $schemaSet = new as_SchemaSet();
        $schema = false;
        $create = false;

        if($currentId !== null){
            $schema = $schemaSet->get($schemaSet->id->is($currentId));
        }

        if(!$schema){

            $schema = $schemaSet->get(
                $schemaSet->ov_schema->is($schemaId)
                ->_AND_($schemaSet->type->is($type))
                ->_AND_($schemaSet->user->is(bab_getUserId()))
            );

            if(!$schema){
                $create = true;
                $schema = $schemaSet->newRecord();
            }
        }else{
            return $schema;
        }

        $schema->type = $type;
        $schema->user = bab_getUserId();
        $schema->ov_schema = $schemaId;

        if($schemaId){
            $ASInfo = bab_WFGetApprobationInfos($schemaId);
            if($ASInfo['forder'] == 'Y'){
                $schema->follow_order = true;
            }else{
                $schema->follow_order = false;
            }

            $schema->name = $ASInfo['name'];
            $schema->description = $ASInfo['description'];
            $schema->org_chart = $ASInfo['id_oc'];

            if($schema->type != $ASInfo['satype']){
                throw new bab_InvalidActionException();
            }
        }else{
            $schema->name = '';
            $schema->description = '';
            //$schema->org_chart = 0;
            $ASInfo = false;
            $schema->follow_order = true;
        }

        $schema->save();

        if($ASInfo && $create){
            $this->convertSchema($ASInfo['formula'], $schema);
        }

        return $schema;
    }


    private function getEntity($entity, $type)
    {
        $W = bab_Widgets();

        $name = as_getEntityName($type, $entity->entity);

        switch($type){
            case as_SchemaSet::type_function:
                $confimMsg = as_translate('This will remove the function "%s" for this step for futur approbation instance using this schema, proceed?');
                break;
            case as_SchemaSet::type_group:
                $confimMsg = as_translate('This will remove the group "%s" for this step for futur approbation instance using this schema, proceed?');
                break;
            case as_SchemaSet::type_name:
            default:
                $confimMsg = as_translate('This will remove the user "%s" for this step for futur approbation instance using this schema, proceed?');
        }

        $layout = $W->HBoxItems(
            $W->Link(
                $W->Icon('', Func_Icons::ACTIONS_EDIT_DELETE),
                as_Controller()->Schema()->deleteEntity($entity->id)
            )->setAjaxAction(as_Controller()->Schema()->deleteEntity($entity->id))
            ->setConfirmationMessage(
                sprintf(
                    $confimMsg,
                    $name
                )
            ),
            $W->Label($name)
        )->setVerticalAlign('middle')->setHorizontalSpacing(.1,'em');

        return $layout;
    }


    public function getStep($step, $type, $followorder)
    {
        
        if (!bab_isUserAdministrator() && !bab_isDelegated('approbations')) {
            throw new bab_AccessException(as_translate('Access denied'));
        }
        
        $W = bab_Widgets();

        $stepSet = new as_SchemaStepSet();

        $stepOrm = $stepSet->get($stepSet->id->is($step));
        $all = $stepOrm->all;

        $entitySet = new as_SchemaStepEntitySet();

        $order = $stepOrm->order;

        $entities = $entitySet->select($entitySet->step->is($step));

        $header = as_translate('Step') . ' #'. $order;

        $stepW = $W->Section(
            $header,
            $W->FlowItems()->setVerticalAlign('top')->setVerticalSpacing(.25, 'em')->setHorizontalSpacing(1, 'em')
        )->setId('step-'.$step)->addClass('widget-bordered');

        $first = true;
        foreach($entities as $entity){
            if($first === true){
                $first = false;
            }else{
                if($all){
                    $stepW->addItem($W->Title(as_translate('And'), 4));
                }else{
                    $stepW->addItem($W->Title(as_translate('Or'), 4));
                }
            }

            $stepW->addItem($this->getEntity($entity, $type));
        }

        $menu = $stepW->addContextMenu();

        $menu->addClass('widget-nowrap');

        if($all){
            $toggleUser = sprintf(as_translate('Validation with %s'), as_getEntityTypeName($type));
        }else{
            if($type == 1){
                $toggleUser = as_translate('Validation with all fonctions');
            }else{
                $toggleUser = sprintf(as_translate('Validation with all %s'), as_getEntityTypeName($type, true));;
            }
        }

        if($entities && $entities->count() >1){
            $menu->addItem(
                $W->Link(
                    $W->Icon(
                        $toggleUser,
                        Func_Icons::CATEGORIES_PREFERENCES_OTHER
                    )->addClass('widget-nowrap'),
                    as_Controller()->Schema()->addStep('$schema')
                )->setAjaxAction(as_Controller()->Schema()->toggleStep($step), $stepW)
            );
        }

        $menu->addItem(
            $W->Link(
                $W->Icon(
                    as_translate('Delete this step'),
                    Func_icons::ACTIONS_EDIT_DELETE
                )->addClass('widget-nowrap'),
                as_Controller()->Schema()->deleteStep($step)
            )->setConfirmationMessage(
                sprintf(
                    as_translate('This will also remove all linked %s'),
                    as_getEntityTypeName($type, true)
                )
            )
            ->setAjaxAction(as_Controller()->Schema()->deleteStep($step), 'as-steps-container')
        );

        $stepW->addItem(
            $W->Link(
                $W->Icon(
                    sprintf(
                        as_translate('Add %s'),
                        as_getEntityTypeName($type)
                    ),
                    Func_icons::ACTIONS_LIST_ADD
                ),
                as_Controller()->Schema()->addEntity($step, $type)
            )->setOpenMode(Widget_Link::OPEN_DIALOG_AND_RELOAD)
        );

        $order++;

        return $stepW->setReloadAction(as_Controller()->Schema(true)->getStep($step, $type, $followorder));
    }


    public function getSteps($schema, $type, $followorder)
    {
        
        if (!bab_isUserAdministrator() && !bab_isDelegated('approbations')) {
            throw new bab_AccessException(as_translate('Access denied'));
        }
        
        $W = bab_Widgets();
        $stepSet = new as_SchemaStepSet();

        $steps = $stepSet->select($stepSet->schema->is($schema));
        $steps->orderAsc($stepSet->order);

        $layout = $W->VBoxLayout('as-steps-container')->setVerticalSpacing(1, 'em');

        foreach($steps as $step){
            $layout->addItem(
                $W->DelayedItem(as_Controller()->Schema(true)->getStep($step->id, $type, $followorder))
            );
        }

        $layout->addItem(
            $W->Link(
                $W->Icon(
                    as_translate('Add a step'),
                    Func_icons::ACTIONS_LIST_ADD
                ),
                as_Controller()->Schema()->addStep($schema)
            )->setAjaxAction(as_Controller()->Schema()->addStep($schema), 'as-steps-container')
        );

        return $layout->setReloadAction(as_Controller()->Schema(true)->getSteps($schema, $type, $followorder));
    }


    /**
     * @AJAX
     */
    public function addStep($schema){
        
        if (!bab_isUserAdministrator() && !bab_isDelegated('approbations')) {
            throw new bab_AccessException(as_translate('Access denied'));
        }
        
        $set = new as_SchemaStepSet();

        $steps = $set->select($set->schema->is($schema));
        $max = 0;
        foreach($steps as $step){
            if($max < $step->order){
                $max = $step->order;
            }
        }
        $max++;

        $step = $set->newRecord();
        $step->schema = $schema;
        $step->order = $max;
        $step->all = false;

        $step->save();

        return true;
    }


    /**
     * @AJAX
     */
    public function deleteStep($step){
        
        if (!bab_isUserAdministrator() && !bab_isDelegated('approbations')) {
            throw new bab_AccessException(as_translate('Access denied'));
        }
        
        $stepSet = new as_SchemaStepSet();
        $entitySet = new as_SchemaStepEntitySet();

        $entitySet->delete($entitySet->step->is($step));
        $stepSet->delete($stepSet->id->is($step));

        return true;
    }


    /**
     * @AJAX
     */
    public function toggleStep($step){
        
        if (!bab_isUserAdministrator() && !bab_isDelegated('approbations')) {
            throw new bab_AccessException(as_translate('Access denied'));
        }
        
        $set = new as_SchemaStepSet();

        $step = $set->get($set->id->is($step));
        if($step){
            if($step->all){
                $step->all = false;
            }else{
                $step->all = true;
            }

            $step->save();
        }

        return true;
    }


    /**
     * @AJAX
     */
    public function addEntity($step, $type){
        
        if (!bab_isUserAdministrator() && !bab_isDelegated('approbations')) {
            throw new bab_AccessException(as_translate('Access denied'));
        }
        
        return new as_entityEditor($step, $type);
    }

    /**
     * @AJAX
     */
    public function saveEntity($entity = null)
    {
        if (!bab_isUserAdministrator() && !bab_isDelegated('approbations')) {
            throw new bab_AccessException(as_translate('Access denied'));
        }
        
        $set = new as_SchemaStepEntitySet();

        $arelady = $set->get(
            $set->step->is($entity['step'])
            ->_AND_($set->entity->is($entity['id']))
        );
        if($arelady){
            return true;
        }

        $entityStep = $set->newRecord();
        $entityStep->step = $entity['step'];
        $entityStep->entity = $entity['id'];
        $entityStep->save();

        return true;
    }

    /**
     * @AJAX
     */
    public function deleteEntity($entityId = null)
    {
        if (!bab_isUserAdministrator() && !bab_isDelegated('approbations')) {
            throw new bab_AccessException(as_translate('Access denied'));
        }
        
        $set = new as_SchemaStepEntitySet();

        $set->delete($set->id->is($entityId));

        return true;
    }

    public function orgEdit($id){
        if (!bab_isUserAdministrator() && !bab_isDelegated('approbations')) {
            throw new bab_AccessException(as_translate('Access denied'));
        }
        
        $schemaSet = new as_SchemaSet();
        $schema = $schemaSet->get($schemaSet->id->is($id));

        return new as_SchemaOrgEditor($schema);
    }

    public function edit($type = as_SchemaSet::type_name, $schemaId = 0, $currentId = null)
    {
        if (!bab_isUserAdministrator() && !bab_isDelegated('approbations')) {
            throw new bab_AccessException(as_translate('Access denied'));
        }
        $W = bab_Widgets();

        $page = $W->BabPage();
        $page->addStyleSheet('addons/approbation_schema/approbation_schema.css');

        $page->addItemMenu('approbation_schema', as_translate('Schemas list'), as_Controller()->schema()->displayList()->url());
        if($type == as_SchemaSet::type_name) {
            if($schemaId != 0){
                $page->addItemMenu('nominative', as_translate('Update nominative schema'), as_Controller()->schema()->edit($type, $schemaId, $currentId)->url());
            }else{
                $page->addItemMenu('nominative', bab_translate('Nominative schema'), as_Controller()->schema()->edit($type, $schemaId, $currentId)->url());
            }
            $page->setCurrentItemMenu('nominative');
        } else if($type == as_SchemaSet::type_function) {
            if($schemaId != 0){
                $page->addItemMenu('staff', as_translate('Update staff schema'), as_Controller()->schema()->edit($type, $schemaId, $currentId)->url());
            }else{
                $page->addItemMenu('staff', bab_translate('Staff schema'), as_Controller()->schema()->edit($type, $schemaId, $currentId)->url());
            }
            $page->setCurrentItemMenu('staff');
        } else if($type == as_SchemaSet::type_group) {
            if($schemaId != 0){
                $page->addItemMenu('group', as_translate('Update group schema'), as_Controller()->schema()->edit($type, $schemaId, $currentId)->url());
            }else{
                $page->addItemMenu('group', bab_translate('Group schema'), as_Controller()->schema()->edit($type, $schemaId, $currentId)->url());
            }
            $page->setCurrentItemMenu('group');
        }

        $schema = $this->getNewSchema($schemaId, $currentId, $type);

        if($schema->org_chart == 0 && $type == as_SchemaSet::type_function){
            $editor = $this->orgEdit($schema->id);
        }else{
            $editor = new as_SchemaEditor($schema);
        }

        $page->addItem($editor);

        return $page;
    }


    private function deleteDraftSchema($schemaId)
    {
        $schemaSet = new as_SchemaSet();
        $schemaStepSet = new as_SchemaStepSet();
        $schemaStepEntitySet = new as_SchemaStepEntitySet();

        $schema = $schemaSet->get($schemaSet->id->is($schemaId));
        $schemaSteps = $schemaStepSet->select($schemaStepSet->schema->is($schemaId));
        foreach($schemaSteps as $schemaStep){
            $schemaStepEntitySet->delete($schemaStepEntitySet->step->is($schemaStep->id));
        }
        $schemaStepSet->delete($schemaStepSet->schema->is($schemaId));
        $schemaSet->delete($schemaSet->id->is($schemaId));
    }

    public function cancelSchema($schemaId)
    {
        if (!bab_isUserAdministrator() && !bab_isDelegated('approbations')) {
            throw new bab_AccessException(as_translate('Access denied'));
        }
        
        $this->deleteDraftSchema($schemaId);

        as_Controller()->Schema()->displayList()->location();
    }


    public function save($schema = null)
    {
        if (!bab_isUserAdministrator() && !bab_isDelegated('approbations')) {
            throw new bab_AccessException(as_translate('Access denied'));
        }
        
        $schemaSet = new as_SchemaSet();
        $record = $schemaSet->get($schemaSet->id->is($schema['id']));
        $record->name = $schema['name'];
        $record->description = $schema['description'];
        $record->follow_order = true;

        $record->save();

        $result = $record->sendToOVidentia();

        if(!$result){
            return false;
        }

        $this->deleteDraftSchema($schema['id']);
        return true;
    }


    public function saveOrg($schema = null)
    {
        if (!bab_isUserAdministrator() && !bab_isDelegated('approbations')) {
            throw new bab_AccessException(as_translate('Access denied'));
        }
        
        $schemaSet = new as_SchemaSet();
        $record = $schemaSet->get($schemaSet->id->is($schema['id']));
        $record->org_chart = $schema['org_chart'];

        $record->save();

        as_Controller()->Schema()->edit(as_SchemaSet::type_function, null, $record->id)->location();
    }


    public function delete($schemaId = null)
    {
        if (!bab_isUserAdministrator() && !bab_isDelegated('approbations')) {
            throw new bab_AccessException(as_translate('Access denied'));
        }
        
        if(bab_WFIsApprobationInUse($schemaId) === false){
            $schemaSet = new as_SchemaSet();
            $schemas = $schemaSet->select($schemaSet->ov_schema->is($schemaId));
            foreach($schemas as $schema){
                $this->deleteDraftSchema($schema->id);
            }


            bab_WFDeleteSchema($schemaId);
        }

        return true;
    }


    public function cancel()
    {
        return true;
    }
}


