<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../functions.php';
require_once $GLOBALS['babInstallPath']."utilit/dateTime.php";


bab_Widgets()->includePhpClass('widget_Form');
bab_Widgets()->includePhpClass('Widget_BabTableArrayView');


class as_TableArrayView extends Widget_BabTableArrayView
{
    protected $pageLength = 20;

    public function computeCellContent($content, $key, $id)
    {
        $W = bab_Widgets();
        static $controller = null;
        static $schemaSet = null;
        if($controller == null){
            $schemaSet = new as_SchemaSet();
            $controller = as_Controller()->schema();
        }

        static $types = null;
        if($types == null){
            $types = array(
                '0' => bab_translate('Nominative schema'),
                '1' => bab_translate('Staff schema'),
                '2' => bab_translate('Group schema')
            );
        }

        if($key == '_actions')
        {
            $layout = $W->HBoxItems()->setHorizontalSpacing(1, 'em');

            $editCtrl = $controller->edit($content['type'], $id);

            $schema = $schemaSet->get(
                $schemaSet->ov_schema->is($id)
                ->_AND_($schemaSet->user->is(bab_getUserId()))
            );

            if($schema){
                $layout->addItem(
                    $W->Link(
                        $W->Icon('', Func_Icons::ACTIONS_DIALOG_CANCEL),
                        $controller->cancelSchema($schema->id)
                    )->setConfirmationMessage(as_translate('This will permantly remove all unsaved update done on this schema.'))->setTitle(as_translate('Cancel all schema modification'))
                );
                $layout->addItem(
                    $W->Link(
                        $W->Icon(as_translate('Resume schema edition'), Func_Icons::ACTIONS_DOCUMENT_EDIT),
                        $editCtrl
                    )->setTitle(as_translate('Resume schema edition'))
                );
            }else{
                $layout->addItem(
                    $W->Link(
                        $W->Icon('', Func_Icons::ACTIONS_DOCUMENT_EDIT),
                        $editCtrl
                    )->setTitle(as_translate('Update this schema'))
                );
            }

            if(bab_WFIsApprobationInUse($id) === false){
                $layout->addItem(
                    $W->Link(
                        $W->Icon('', Func_Icons::ACTIONS_EDIT_DELETE),
                        $controller->delete($id)
                    )->setTitle(as_translate('Remove this scheme'))
                    ->setConfirmationMessage(as_translate('This will remove this scheme.'))
                );
            }

            return $layout;
        }

        if($key == 'type')
        {
            return $W->Label($types[$content[$key]]);
        }

        if($key == 'id_oc')
        {
            if($content[$key] != 0){
                $oc = bab_OCGetOrgChart($content[$key]);
                return $W->Label($oc['name']);
            }else{
                return $W->Label('');
            }
        }

        return $W->Label($content[$key]);
    }
}



/**
 * Formulaire
 *
 */
class as_SchemaEditor extends Widget_Form {

    protected $schema = false;


    public function __construct($schema)
    {
        $W = bab_Widgets();

        $this->schema = $schema;

        parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->setName('schema');
        $this->colon();

        $this->addClass('widget-bordered');
        $this->addClass(func_icons::ICON_LEFT_16);
        $this->addClass('widget-centered');
        $this->addClass('BabLoginMenuBackground');

        $this->addDefaultFields();

        $this->addFields();

        $this->addInfo();

        $this->addButtons();

        $this->setHiddenValue('schema[id]', $this->schema->id);



        $this->setValues(
            array(
                'schema' => array(
                    'name' => $this->schema->name,
                    'description' => $this->schema->description,
                    'follow_order' => $this->schema->follow_order
                )
            )
        );

    }


    protected function addSteps(){
        $W = bab_Widgets();

        $this->addItem(
            $W->DelayedItem(as_Controller()->Schema()->getSteps($this->schema->id, $this->schema->type, $this->schema->follow_order))
        );
    }


    protected function addFields()
    {
        $W = bab_Widgets();

        $this->addSteps();
    }

    protected function addInfo()
    {
        $W = bab_Widgets();
        $this->addItem(
            $W->HBoxItems(
                $W->Icon('', Func_Icons::STATUS_DIALOG_INFORMATION),
                $W->Label(as_translate('All step must be complete if you want that your approbation to be sucessful.'))
            )
        );
    }

    protected function name(){
        $W = bab_Widgets();

        return $W->LabelledWidget(
            as_translate('Name'),
            $W->LineEdit()->setMandatory(true, as_translate('The name is mandatory.')),
            'name'
        );
    }

    protected function description(){
        $W = bab_Widgets();

        return $W->LabelledWidget(
            as_translate('Description'),
            $W->LineEdit()->setSize(60),
            'description'
        );
    }

    protected function org_chart(){
        $W = bab_Widgets();

        if($this->schema->type != as_SchemaSet::type_function){
            return null;
        }

        $orgs = bab_OCGetAccessibleOrgCharts(3);
        $select = array();
        foreach($orgs as $org) {
            $select[$org['id']] = $org['name'];
        }

        return $W->LabelledWidget(
            as_translate('Orgchart'),
            $W->Select()->setValue($this->schema->org_chart)->setOptions($select),
            'org_chart'
        )->setDisplayMode();
    }

    protected function follow_order(){
        $W = bab_Widgets();
        if(!$this->schema->follow_order){
            $this->addItem(
                $W->Icon(
                    as_translate("Warning, the follow order option was not enable please make sur the approbation schema is correctly filed if it is not, try to make it match the new template."),
                    Func_Icons::STATUS_DIALOG_WARNING
                )
            );
            /*return $W->LabelledWidget(
                as_translate('Follow order'),
                $W->CheckBox(),
                'follow_order'
            );*/
        }else{
            return null;
        }
    }

    protected function addDefaultFields()
    {
        $W = bab_Widgets();

        $this->follow_order();

        $this->addItem(
            $W->FlowItems(
                $this->name(),
                $this->description(),
                $this->org_chart()
            )->setVerticalAlign('bottom')->setHorizontalSpacing(1.5, 'em')
        );
    }


    protected function addButtons()
    {
        $W = bab_Widgets();

        $ctrl = as_Controller()->Schema();
        $saveAction = $ctrl->save();

        $save = $W->SubmitButton()->setLabel(as_translate('Save'))
            ->setAction($saveAction)
            ->setSuccessAction($ctrl->displayList())
            ->setFailedAction($ctrl->edit());

        $this->addItem(
            $W->FlowItems(
                $save,
                $W->Link(
                    $W->Icon(as_translate('Cancel all modification'),Func_Icons::ACTIONS_DIALOG_CANCEL),
                    $ctrl->cancelSchema($this->schema->id)
                )->setConfirmationMessage(as_translate('This will delete all modifications done on the approbation shema'))
            )->setHorizontalSpacing(1,'em')
        );
    }
}



/**
 * Formulaire
 *
 */
class as_entityEditor extends Widget_Form {

    protected $step = false;
    protected $type = false;


    public function __construct($step, $type)
    {
        $W = bab_Widgets();

        $this->step = $step;
        $this->type = $type;

        parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->setName('entity');
        $this->colon();

        $this->addClass('widget-bordered');
        $this->addClass(func_icons::ICON_LEFT_16);
        $this->addClass('widget-centered');
        $this->addClass('BabLoginMenuBackground');

        $this->addFields();

        $this->setHiddenValue('entity[step]', $step);

        $this->addButtons();
    }


    protected function addFunction()
    {
        $W = bab_Widgets();

        $stepSet = new as_SchemaStepSet();
        $stepSet->schema();
        $step = $stepSet->get($stepSet->id->is($this->step));

        $this->addItem(
            $W->LabelledWidget(
                as_translate('User'),
                $W->FunctionPicker()->setRelative()->setOrgchart($step->schema->org_chart)->setName('id')
            )
        );
    }


    protected function addGroup()
    {
        $W = bab_Widgets();

        $this->addItem(
            $W->LabelledWidget(
                as_translate('Group'),
                $W->GroupPicker()->setStartGroup(1)->setName('id')
            )
        );
    }


    protected function addUser()
    {
        $W = bab_Widgets();

        $this->addItem(
            $W->LabelledWidget(
                as_translate('User'),
                $W->UserPicker()->setName('id')
            )
        );
    }


    protected function addFields()
    {
        $W = bab_Widgets();

        switch($this->type)
        {
            case as_SchemaSet::type_function:
               $this->addFunction();
                break;
            case as_SchemaSet::type_group:
                $this->addGroup();
                break;
            case as_SchemaSet::type_name:
            default:
                $this->addUser();
        }
    }


    protected function addButtons()
    {
        $W = bab_Widgets();

        $ctrl = as_Controller()->Schema();
        $saveAction = $ctrl->saveEntity();

        $save = $W->SubmitButton()->setLabel(as_translate('Save'))
            ->setAjaxAction($saveAction);

        $this->addItem(
            $save
        );
    }
}



/**
 * Formulaire
 *
 */
class as_SchemaOrgEditor extends Widget_Form {

    protected $schema = false;


    public function __construct($schema)
    {
        $W = bab_Widgets();

        $this->schema = $schema;

        parent::__construct(null, $W->VBoxLayout()->setVerticalSpacing(1, 'em'));

        $this->setHiddenValue('tg', bab_rp('tg'));
        $this->setName('schema');
        $this->colon();

        $this->addClass('widget-bordered');
        $this->addClass(func_icons::ICON_LEFT_16);
        $this->addClass('widget-centered');
        $this->addClass('BabLoginMenuBackground');

        $this->addFields();

        $this->setHiddenValue('schema[id]', $schema->id);

        $this->addButtons();
    }


    protected function addFields()
    {
        $W = bab_Widgets();

        $orgs = bab_OCGetAccessibleOrgCharts(3);
        $select = array();
        foreach($orgs as $org) {
            $select[$org['id']] = $org['name'];
        }

        $this->addItem(
            $W->LabelledWidget(
                bab_translate("Select the organizational chart on which the workflow will be based"),
                $W->Select()->setOptions($select)->setName('org_chart')
            )
        );
    }


    protected function addButtons()
    {
        $W = bab_Widgets();

        $ctrl = as_Controller()->Schema();
        $saveAction = $ctrl->saveOrg();

        $save = $W->SubmitButton()->setLabel(as_translate('Validate'))
            ->setAction($saveAction);

        $this->addItem(
            $save
        );
    }
}
