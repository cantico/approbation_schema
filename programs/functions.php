<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

function as_translate($str, $str_plurals = null, $number = null)
{
    if ($translate = bab_functionality::get('Translate/Gettext'))
    {
        /* @var $translate Func_Translate_Gettext */
        $translate->setAddonName('approbation_schema');

        return $translate->translate($str, $str_plurals, $number);
    }

    return $str;
}


/**
 * Initialize mysql ORM backend.
 */
function as_loadOrm()
{
    if (!class_exists('ORM_MySqlRecordSet'))
    {
        $Orm = bab_functionality::get('LibOrm');
        /*@var $Orm Func_LibOrm */
        $Orm->initMySql();

        $mysqlbackend = new ORM_MySqlBackend($GLOBALS['babDB']);
        ORM_MySqlRecordSet::setBackend($mysqlbackend);
    }
}


/**
 * @return as_Controller
 */
function as_Controller()
{
    require_once dirname(__FILE__) . '/controller.class.php';
    return bab_getInstance('as_Controller');
}


/**
 * @return bool
 */
function as_isManager()
{
    return bab_isUserAdministrator();
}


/**
 * @return bool
 */
function as_isViewer()
{
    return false;
}


function as_getEntityName($type, $id)
{
    switch($type)
    {
        case as_SchemaSet::type_function:
            global $babDB;

            if($id==0){
                return bab_translate("Immediat superior");
            }
            if($id == -1){
                return sprintf(bab_translate("Level %d superior"), 2);
            }
            if($id == -2){
                return sprintf(bab_translate("Level %d superior"), 3);
            }

            $role = bab_OCGetRole($id);

            if($role){
                $sql = bab_OCGetPathToNodeQuery($role['id_entity'], true);
                $entities = $babDB->db_query($sql);
                $path = '';
                while($entity = $babDB->db_fetch_assoc($entities)){
                    $path.= $entity['sName'] .' > ';
                }

                return $path . $role['name'];
            }else{
                return as_translate('(Role deleted)');
            }
            break;
        case as_SchemaSet::type_group:
            return bab_getGroupName($id);
            break;
        case as_SchemaSet::type_name:
        default:
            $name = bab_getUserName($id);
            if ($name === false) {
                return as_translate('(User deleted)');
            }
            return $name;
    }
}


function as_getEntityTypeName($type, $s = false)
{
    if($s){
        switch($type)
        {
            case as_SchemaSet::type_function:
               return as_translate('functions');
                break;
            case as_SchemaSet::type_group:
                return as_translate('groups');
                break;
            case as_SchemaSet::type_name:
            default:
                return as_translate('users');
        }
    }
    switch($type)
    {
        case as_SchemaSet::type_function:
           return as_translate('a function');
            break;
        case as_SchemaSet::type_group:
            return as_translate('a group');
            break;
        case as_SchemaSet::type_name:
        default:
            return as_translate('a user');
    }
}
