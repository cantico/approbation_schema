<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
include "base.php";


function approbation_schema_onDeleteAddon()
{
    require_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';
    require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

    bab_removeAddonEventListeners('approbation_schema');

    return true;
}


function approbation_schema_upgrade($version_base, $version_ini)
{
    require_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';
    require_once $GLOBALS['babInstallPath'].'utilit/devtools.php';
    require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

    require_once dirname(__FILE__).'/set/schema.class.php';
    require_once dirname(__FILE__).'/set/schemastep.class.php';
    require_once dirname(__FILE__).'/set/schemastepentity.class.php';

    $addon = bab_getAddonInfosInstance('approbation_schema');
    $addon->removeAllEventListeners();
    $addon->addEventListener('bab_eventBeforeSiteMapCreated', 'approbation_schema_onBeforeSiteMapCreated', 'events.php');

    $schemaSet = new as_SchemaSet();
    $schemaStepSet = new as_SchemaStepSet();
    $schemaStepEntitySet = new as_SchemaStepEntitySet();

    $synchronize = new bab_synchronizeSql();
    $synchronize->addOrmSet($schemaSet);
    $synchronize->addOrmSet($schemaStepSet);
    $synchronize->addOrmSet($schemaStepEntitySet);
    $synchronize->updateDatabase();

    return true;
}
